// Helper function for generating random colors for blocks
function randomColor() {
  const hue = Math.random() * 360;
  return `hsl(${hue},100%,50%)`;
}

// Options for each of the 3 intersection observers
const blockOptions = {
  root: null,
  threshold: 0.01,
  rootMargin: "0px", // Animation is run as the element enter the viewport
};

const imageOptions = {
  root: null,
  threshold: 0.01,
  rootMargin: "200px", // Loads image 200px before it enters the viewport
};
const aniOptions = {
  root: null,
  threshold: 0.01,
  rootMargin: "-50px", // Runs animation 50px after the element has entered the viewport
};

// Callbacks for each intersection observer
function onBlockIntersection(entries) {
  entries.forEach(entry => {
    if (entry.intersectionRatio > 0) {
      blockObserver.unobserve(entry.target);
      entry.target.style.backgroundColor = randomColor();
      entry.target.classList.add("show");
    }
  });
}

function onImageIntersection(entries) {
  entries.forEach(entry => {
    if (entry.intersectionRatio > 0) {
      if (entry.target.dataset.src === entry.target.src) return;
      imageObserver.unobserve(entry.target);
      entry.target.src = entry.target.dataset.src;
    }
  });
}

function onAniEnterIntersection(entries) {
  entries.forEach(entry => {
    if (entry.intersectionRatio > 0) {
      aniEnterObserver.unobserve(entry.target);
      entry.target.classList.add("enter");
    }
  });
}

// Initializing each of the observers with their callbacks and options
const blockObserver = new IntersectionObserver(onBlockIntersection, blockOptions);
const imageObserver = new IntersectionObserver(onImageIntersection, imageOptions);
const aniEnterObserver = new IntersectionObserver(onAniEnterIntersection, aniOptions);

// Selectors for our 3 observed element types
const blocks = document.querySelectorAll(".block");
const images = document.querySelectorAll(".lazy-img");
const animatedIn = document.querySelectorAll(".ani-enter");

// Adding each of the selected elements to their appropriate interseciton observer
blocks.forEach(block => {
  blockObserver.observe(block);
});

images.forEach(image => {
  imageObserver.observe(image);
});

animatedIn.forEach(item => {
  aniEnterObserver.observe(item);
});

// Funciton to make final image appear
const showButton = document.getElementById("showButton");
showButton.addEventListener("click", () => {
  document.querySelector(".hide").classList.remove("hide");
});

// BONUS => This set up can be used to asyncronously load images after the inital paint
const asyncImages = document.querySelectorAll(".async");
asyncImages.forEach(image => {
  if (image.dataset.src === image.src) return;
  const img = new Image();
  img.src = image.dataset.src;
  img.onload = () => {
    image.src = img.src;
  };
});
